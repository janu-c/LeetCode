/*
 * @lc app=leetcode.cn id=233 lang=cpp
 *
 * [233] 数字 1 的个数
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
    int cntDig1_nBits(int n) { return n * pow(10, n - 1); } //计算n位及以下的数含有的1的个数

public:
    int countDigitOne(int n)
    {
        int result = 0;
        vector<int> bitN;

        for (int m = n; m > 0; m /= 10)
            bitN.push_back(m % 10);

        for (int i = bitN.size() - 1; i >= 0; i--)
        {
            result += bitN[i] * cntDig1_nBits(i);
            if (bitN[i] == 1)
                result += n % (bitN[i] * (int)pow(10, i)) + 1;
            else if (bitN[i] > 1)
                result += pow(10, i);
        }
        return result;
    }
};
// @lc code=end
