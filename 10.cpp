#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

struct
{
    bool operator()(const string &s1, const string &s2) const
    {
        return s1.length() < s2.length();
    }
} min_size;

struct
{
    char ch;
    int index;
    bool operator()(const string &s) const
    {
        if (index >= s.length())
            return false;
        return s[index] == ch;
    }
} is_common_char;

int main()
{
    vector<string> strs =
        {"flower", "flow", "flight"};
    // {"dog", "rececar", "car"};
    string result("");

    int i = 0;
    do
    {
        is_common_char.ch = strs[0][i];
        is_common_char.index = i;
        if (!all_of(strs.begin() + 1, strs.end(), is_common_char))
            break;
    } while (++i);
    result = i == -1 ? "" : strs[0].substr(0, i);
    cout << result << endl;

    return 0;
}