/*
 * @lc app=leetcode.cn id=2399 lang=cpp
 *
 * [2399] 检查相同字母间的距离
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
public:
    bool checkDistances(string s, vector<int> &distance)
    {
        string::iterator appear1, appear2;
        for (int i = 0; i < distance.size(); i++)
        {
            if ((appear1 = find(s.begin(), s.end(), 'a' + i)) == s.end())
                continue;
            appear2 = find(appear1 + 1, s.end(), 'a' + i);
            if (appear2 - appear1 - 1 != distance[i])
                return 0;
        }
        return 1;
    }
};
// @lc code=end
