/*
 * @lc app=leetcode.cn id=14 lang=cpp
 *
 * [14] 最长公共前缀
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
public:
    struct
    {
        char ch;
        int index;
        bool operator()(const string &s) const
        {
            if (index >= s.length())
                return false;
            return s[index] == ch;
        }
    } is_common_char;

    string longestCommonPrefix(vector<string> &strs)
    {
        if (strs.size() == 1)
            return strs[0];
        int i = 0;
        do
        {
            is_common_char.ch = strs[0][i];
            is_common_char.index = i;
            if (!all_of(strs.begin() + 1, strs.end(), is_common_char))
                break;
        } while (++i);
        string result = i == -1 ? "" : strs[0].substr(0, i);
        return result;
    }
};
// @lc code=end
