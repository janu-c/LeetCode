/*
 * @lc app=leetcode.cn id=1622 lang=cpp
 *
 * [1622] 奇妙序列
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Fancy
{
    vector<int> a;

    vector<pair<bool, int>> calculations;
    size_t cal_cnt = 0;
    vector<int> cal_record; //下标为i的元素表示第i个元素插入前已进行计算的次数

public:
    Fancy()
    {
    }

    void append(int val)
    {
        a.push_back(val);
        cal_record.push_back(cal_cnt);
    }

    void addAll(int inc)
    {
        ++cal_cnt;
        calculations.push_back({1, inc});
    }

    void multAll(int m)
    {
        ++cal_cnt;
        calculations.push_back({0, m});
    }

    int getIndex(int idx)
    {
        if (idx >= a.size())
            return -1;

        const int DIVISOR = 1e9 + 7;
        int start = cal_record[idx];
        long long result = a[idx];

        // for (int i = 0; i < calculations.size() - start; i++)
        //     cout << '(';
        // cout << result;
        // for (auto it = calculations.begin() + start; it < calculations.end(); it++)
        // {
        //     if ((*it).first)
        //         cout << '+';
        //     else
        //         cout << '*';
        //     cout << (*it).second << ')';
        // }
        // cout << endl;

        for (auto it = calculations.begin() + start; it < calculations.end(); it++)
        {
            if ((*it).first)
                result = (result % DIVISOR + (*it).second % DIVISOR) % DIVISOR;
            else
                result = ((result % DIVISOR) * ((*it).second % DIVISOR)) % DIVISOR;
        }

        return result;
    }
};

/**
 * Your Fancy object will be instantiated and called as such:
 * Fancy* obj = new Fancy();
 * obj->append(val);
 * obj->addAll(inc);
 * obj->multAll(m);
 * int param_4 = obj->getIndex(idx);
 */
// @lc code=end
