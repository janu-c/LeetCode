/*
 * @lc app=leetcode.cn id=432 lang=cpp
 *
 * [432] 全 O(1) 的数据结构
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class AllOne
{
    
public:
    AllOne()
    {
    }

    void inc(string key)
    {
    }

    void dec(string key)
    {
    }

    string getMaxKey()
    {
    }

    string getMinKey()
    {
    }
};

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne* obj = new AllOne();
 * obj->inc(key);
 * obj->dec(key);
 * string param_3 = obj->getMaxKey();
 * string param_4 = obj->getMinKey();
 */
// @lc code=end
