/*
 * @lc app=leetcode.cn id=204 lang=cpp
 *
 * [204] 计数质数
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
    vector<int> factors;
    void findPrimesFactors(const int n)
    {
        bool tag;
        for (int i = 2; i < n; i++)
        {
            tag = 1;
            int j = sqrt(i) + 1;
            for (auto it = factors.begin(); it != factors.end() && *it < j; it++)
                if (i % *it == 0)
                {
                    tag = 0;
                    break;
                }
            if (tag)
                factors.push_back(i);
        }
    }

public:
    int countPrimes(int n)
    {
        findPrimesFactors(n);
        return factors.size();
    }
};
// @lc code=end
