/*
 * @lc app=leetcode.cn id=36 lang=cpp
 *
 * [36] 有效的数独
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
    class
    {
        bool val[10];
        const size_t size = sizeof(val) / sizeof(val[0]);

    public:
        bool operator[](int i)
        {
            if (val[i] == 1)
                return 1;
            val[i] = 1;
            return 0;
        }
        void refresh()
        {
            fill_n(val, size, 0);
        }
    } a;

public:
    bool isValidSudoku(vector<vector<char>> &board)
    {
        for (int i = 0; i < 9; i++)
        {
            a.refresh();
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] != '.' && a[board[i][j] - '0'])
                    return 0;
            }
        }
        for (int j = 0; j < 9; j++)
        {
            a.refresh();
            for (int i = 0; i < 9; i++)
            {
                if (board[i][j] != '.' && a[board[i][j] - '0'])
                    return 0;
            }
        }
        for (int k = 0; k < 9; k++)
        {
            int x = k / 3 * 3, y = k % 3 * 3;
            a.refresh();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[x + i][y + j] != '.' && a[board[x + i][y + j] - '0'])
                        return 0;
                }
            }
        }
        return 1;
    }
};
// @lc code=end
