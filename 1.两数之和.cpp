/*
 * @lc app=leetcode.cn id=1 lang=cpp
 *
 * [1] 两数之和
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
public:
    vector<int> twoSum(vector<int> &nums, int target)
    {
        sort(nums.begin(), nums.end());
        int i = *nums.begin(), j = *(nums.end() - 1), mid, tmp;
        while (nums[i] + nums[j] != target)
        {
            mid = i + (j - i) / 2;
            if (nums[i] + nums[j] < target)

                if (nums[mid] + nums[j] < target)
                    i = mid+1;
                    else 
            tmp = target - nums[mid];
        }

        return {i, j};
    }
};
// @lc code=end
