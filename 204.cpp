/*
 *
 * [204] 计数质数
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{

    void findPrimesFactors(const int n)
    {
        bool tag;
        for (int i = 2; i < n; i++)
        {
            tag = 1;
            int j = sqrt(i) + 1;
            for (auto it = primes_factors.begin(); it != primes_factors.end() && *it < j; it++)
                if (i % *it == 0)
                {
                    tag = 0;
                    break;
                }
            if (tag)
                primes_factors.push_back(i);
        }
    }

public:
    vector<int> primes_factors;
    int countPrimes(int n)
    {
        findPrimesFactors(n);
        return primes_factors.size();
    }
};
// @lc code=end

struct : public Solution
{
    char print()
    {
        for (int i = 0; i < primes_factors.size(); i++)
            cout << " " << primes_factors[i];
        cout << endl;
        return '\0';
    }
} s;

int main()
{
    cout << s.countPrimes(0) << endl;
    cout << s.countPrimes(197) << s.print();
    cout << s.countPrimes(999983) << endl;

    // s.countPrimes(999983);

    return 0;
}
