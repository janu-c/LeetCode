#include <bits/stdc++.h>
using namespace std;

class Solution
{
public:
    int countTime(string time)
    {
        int cnt[3] = {1, 1, 1};
        if (time[0] == '?')
        {
            if (time[1] < '4')
                cnt[0] = 3;
            else if (time[1] != '?')
                cnt[0] = 2;
            else
                cnt[0] = 24;
        }
        if (time[1] == '?')
        {
            if (time[0] < '2')
                cnt[1] = 10;
            else if (time[0] == '2')
                cnt[1] = 4;
        }
        if (time[3] == '?')
        {
            if (time[4] == '?')
                cnt[2] = 60;
            else
                cnt[2] = 6;
        }
        else if (time[4] == '?')
            cnt[2] = 10;

        return cnt[0] * cnt[1] * cnt[2];
    }
};

int main()
{
    Solution s;
    cout << s.countTime("?5:00") << endl;
    cout << s.countTime("0?:0?") << endl;
    cout << s.countTime("??:??") << endl;
    cout << s.countTime("07:?3") << endl;
}