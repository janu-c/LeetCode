#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

int main()
{

    string op[] = {"Fancy", "append", "append", "getIndex", "append", "getIndex", "addAll", "append", "getIndex", "getIndex", "append", "append", "getIndex", "append", "getIndex", "append", "getIndex", "append", "getIndex", "multAll", "addAll", "getIndex", "append", "addAll", "getIndex", "multAll", "getIndex", "multAll", "addAll", "addAll", "append", "multAll", "append", "append", "append", "multAll", "getIndex", "multAll", "multAll", "multAll", "getIndex", "addAll", "append", "multAll", "addAll", "addAll", "multAll", "addAll", "addAll", "append", "append", "getIndex"};
    istringstream sin("[[],[12],[8],[1],[12],[0],[12],[8],[2],[2],[4],[13],[4],[12],[6],[11],[1],[10],[2],[3],[1],[6],[14],[5],[6],[12],[3],[12],[15],[6],[7],[8],[13],[15],[15],[10],[9],[12],[12],[9],[9],[9],[9],[4],[8],[11],[15],[9],[1],[4],[10],[9]]");

    const size_t n = sizeof(op) / sizeof(op[0]);
    vector<int> val(1); // val[0] = 0;
    char tmp;
    int num;

    sin >> tmp >> tmp >> tmp >> tmp; //"[[],"
    for (int i = 1; i < n; i++)
    {
        sin >> tmp >> num >> tmp >> tmp;
        val.push_back(num);
    }

    cout << "Fancy *obj = new Fancy();" << op[0] << "();\n";
    for (int i = 1; i < n; i++)
    {
        if (op[i] == "getIndex")
            cout << "cout << ";
        cout << "obj->" << op[i] << '(' << val[i] << ");";
        if (op[i] == "getIndex")
            cout << "\b << endl;";
        cout << endl;
    }
    return 0;
}