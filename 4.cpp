#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <iterator>
#include <iomanip>
using namespace std;

// inline double average(int a, int b)
// {
//     return a + (b - a) / 2;
// }

struct
{
    double operator()(int a, int b) const
    {
        return a + (b - a) / 2;
    }
} average;

inline double find_mid(const vector<int> &v1, const vector<int> &v2, const int l1, const int r1, const int l2, const int r2)
{
    const int mid1 = l1 + (r1 - l1 - 1) / 2, mid2 = l2 + (r2 - l2 - 1) / 2;
    const int &mid1_val = v1[mid1], &mid2_val = v2[mid2];
    if (mid1_val == mid2_val)
        return average(v1[mid1], v2[mid2]);
    if (mid1_val < mid2_val)
    {
        if (l1 < r1)
            return find_mid(v1, v2, mid1 + 1, r1, l2, r2);
        else if (l2 < r2)
            return find_mid(v1, v2, l1, r1, l2, mid2);
        else
            return average(v1[l1], v2[l2]);
    }
    else
    {
        if (l2 < r2)
            return find_mid(v1, v2, l1, r1, mid2 + 1, r2);
        else if (l1 < r1)
            return find_mid(v1, v2, l1, mid1, l2, r2);
        else
            return average(v1[l1], v2[l2]);
    }
}

inline double median(const vector<int> &v1, const vector<int> &v2, const int l1, const int r1, const int l2, const int r2)
{
    
}

int main()
{
    vector<int> nums1, nums2;
    double avr;
    int m, n;
    istringstream sin("1 3 0 2 0 1 2 0 3 4 0");
    int in;
    // ostream_iterator<int> osit(cout, " ");

    while (!sin.eof())
    {
        nums1.clear();
        nums2.clear();
        while (sin >> in, in)
            nums1.push_back(in);
        while (sin >> in, in)
            nums2.push_back(in);

        m = nums1.size(), n = nums2.size();

        avr = find_mid(nums1, nums2, 0, m, 0, n);
        cout << fixed << setprecision(5) << avr << endl;
    }

    return 0;
}
