/*
 *
 * [110] 平衡二叉树
 */
#include <bits/stdc++.h>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
// @lc code=start
class Solution
{
    bool isB = 1; //标志整棵树是否平衡，默认为1（平衡）

    int height_balanced(const TreeNode *t) //求高度，顺便利用高度判断整棵树是否平衡
    {
        if (!t)                            //叶结点
            return 0;                      //高度为0
        int l = height_balanced(t->left);  //求左子树高度
        int r = height_balanced(t->right); //求右子树高度
        isB &= abs(l - r) < 2;             //如果左右子树高度小于2，则以该结点为根结点的子树平衡;若该子树不平衡，则整棵树不平衡
        return 1 + max(l, r);              //该结点的高度为1+左右子树高度的最大值
    }

public:
    bool isBalanced(TreeNode *root)
    {
        height_balanced(root); //表面求高度，实则判断树是否平衡
        return isB;
    }
};
// @lc code=end