/*
 *
 * [50] Pow(x, n)
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Solution
{
    double pow_positive(double x, int n)
    {
        if (x >= INFINITY)
            return INFINITY;
        if (n == 0)
            return 1;
        if (n & 1)
            return x * pow_positive(x, n - 1);
        return pow_positive(x * x, n >> 1);
    }

public:
    double myPow(double x, int n)
    {
        if (n == 0)
            return 1;
        if (x == 0)
            return 0;
        if (x == 1)
            return 1;
        if (x == -1)
            return n & 1 ? -1 : 1;
        if (n > 0)
            return pow_positive(x, abs(n));
        else
            return 1 / pow_positive(x, abs(n));
    }
};
// @lc code=end

int main()
{
    Solution s;
    cout << s.myPow(2, -2) << " " << (-5 & 1) << endl;
    cout << s.myPow(2, 214748648) << " " << s.myPow(2, -214748648) << endl;
    cout << INFINITY * 500 << " " << 1 / INFINITY << endl;

    return 0;
}
