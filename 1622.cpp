/*
 *
 * [1622] 奇妙序列
 */
#include <bits/stdc++.h>
using namespace std;
// @lc code=start
class Fancy
{
    vector<int> a;

    vector<pair<bool, int>> calculations;
    size_t cal_cnt = 0;
    vector<int> cal_record; //下标为i的元素表示第i个元素插入前已进行计算的次数

public:
    Fancy()
    {
    }

    void append(int val)
    {
        a.push_back(val);
        cal_record.push_back(cal_cnt);
    }

    void addAll(int inc)
    {
        ++cal_cnt;
        calculations.push_back({1, inc});
    }

    void multAll(int m)
    {
        ++cal_cnt;
        calculations.push_back({0, m});
    }

    int getIndex(int idx)
    {
        if (idx >= a.size())
            return -1;

        const int DIVISOR = 1e9 + 7;
        int start = cal_record[idx];
        long long result = a[idx];

        // for (int i = 0; i < calculations.size() - start; i++)
        //     cout << '(';
        // cout << result;
        // for (auto it = calculations.begin() + start; it < calculations.end(); it++)
        // {
        //     if ((*it).first)
        //         cout << '+';
        //     else
        //         cout << '*';
        //     cout << (*it).second << ')';
        // }
        // cout << endl;

        for (auto it = calculations.begin() + start; it < calculations.end(); it++)
        {
            if ((*it).first)
                result = (result % DIVISOR + (*it).second % DIVISOR) % DIVISOR;
            else
                result = ((result % DIVISOR) * ((*it).second % DIVISOR)) % DIVISOR;
        }

        return result;
    }
};

/**
 * Your Fancy object will be instantiated and called as such:
 * Fancy* obj = new Fancy();
 * obj->append(val);
 * obj->addAll(inc);
 * obj->multAll(m);
 * int param_4 = obj->getIndex(idx);
 */
// @lc code=end

int main()
{
    Fancy *obj = new Fancy();
    Fancy();
    obj->append(12);
    obj->append(8);
    cout << obj->getIndex(1) << endl;
    obj->append(12);
    cout << obj->getIndex(0) << endl;
    obj->addAll(12);
    obj->append(8);
    cout << obj->getIndex(2) << endl;
    cout << obj->getIndex(2) << endl;
    obj->append(4);
    obj->append(13);
    cout << obj->getIndex(4) << endl;
    obj->append(12);
    cout << obj->getIndex(6) << endl;
    obj->append(11);
    cout << obj->getIndex(1) << endl;
    obj->append(10);
    cout << obj->getIndex(2) << endl;
    obj->multAll(3);
    obj->addAll(1);
    cout << obj->getIndex(6) << endl;
    obj->append(14);
    obj->addAll(5);
    cout << obj->getIndex(6) << endl;
    obj->multAll(12);
    cout << obj->getIndex(3) << endl;
    obj->multAll(12);
    obj->addAll(15);
    obj->addAll(6);
    obj->append(7);
    obj->multAll(8);
    obj->append(13);
    obj->append(15);
    obj->append(15);
    obj->multAll(10);
    cout << obj->getIndex(9) << endl;
    obj->multAll(12);
    obj->multAll(12);
    obj->multAll(9);
    cout << obj->getIndex(9) << endl;
    obj->addAll(9);
    obj->append(9);
    obj->multAll(4);
    obj->addAll(8);
    obj->addAll(11);
    obj->multAll(15);
    obj->addAll(9);
    obj->addAll(1);
    obj->append(4);
    obj->append(10);
    cout << obj->getIndex(9) << endl;

    return 0;
}
